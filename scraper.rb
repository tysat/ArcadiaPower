# When calling script the user and password need to be passed in

require 'watir'

browser = Watir::Browser.new(:firefox)

browser.goto 'https://www.dominionenergy.com/sign-in'

# Locate login form then fill in user/pass using passed in variables
form = browser.form(id: 'MainForm')
form.text_field(name: 'user').set(ARGV[0])
form.text_field(name: 'password').set(ARGV[1])
form.input(value: 'Sign In').click

# Get the due date by class name, it's the first thing (of 2 on the page) that uses this class
duedate = browser.element(class_name: 'bodyTextGreen').text

# Grab all divs with class row contentRowPadding, sort through them to find the one including 'View Bill' (this line has the bill amount) then pull out the billing amount
billamount = 0
divs = browser.divs(class_name: ['row contentRowPadding'])
divs.each do |div|
  if div.text.index('View Bill')
  	billamount = div.text[/[$]([0-9]*[.][0-9]{2})/,1]
  end
end

# Grab the values used to construct the graph, first number after a comma is the kWh from the most recent billing period
usage = browser.hidden(id: 'UsageDataArrHdn').value[/,([0-9]*)/,1]

# Grab the values used in the graph, billing periods are listed most recent to oldest so first two are the end and start of the current period
dates = browser.hidden(id: 'UsageDateArrHdn').value.split(',')
enddate = dates[0]
startdate = dates[1]

# Display the data
puts "Usage (kWh):"
puts usage
puts ''
puts "Bill Amount ($):"
puts billamount
puts ''
puts "Service Start Date:"
puts startdate
puts ''
puts "Service End Date:"
puts enddate
puts ''
puts "Bill Due Date:"
puts duedate

browser.close